// /**
//  * Get the user IP throught the webkitRTCPeerConnection
//  * @param onNewIP {Function} listener function to expose the IP locally
//  * @return undefined
//  */
// function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
//     //compatibility for firefox and chrome
//     var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
//     var pc = new myPeerConnection({
//             iceServers: []
//         }),
//         noop = function() {},
//         localIPs = {},
//         ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
//         key;
//
//     function iterateIP(ip) {
//         if (!localIPs[ip]) onNewIP(ip);
//         localIPs[ip] = true;
//     }
//
//     //create a bogus data channel
//     pc.createDataChannel("");
//
//     // create offer and set local description
//     pc.createOffer().then(function(sdp) {
//         sdp.sdp.split('\n').forEach(function(line) {
//             if (line.indexOf('candidate') < 0) return;
//             line.match(ipRegex).forEach(iterateIP);
//         });
//
//         pc.setLocalDescription(sdp, noop, noop);
//     }).catch(function(reason) {
//         // An error occurred, so handle the failure to connect
//     });
//
//     //listen for candidate events
//     pc.onicecandidate = function(ice) {
//         if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
//         ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
//     };
// }
//
// // Usage
//
// getUserIP(function(ip){
//     alert("Got IP! :" + ip);
// });

class Burger {
        constructor(name, ingredients, time){
            this.name = name || "Burger";
            this.ingredients = ingredients || [
                    'Булка',
                    'Огурчик',
                    'Котлетка',
                    'Кунжут',
            ];
            this.time = time || dTime;
        }
        addMenu(){
            menu.push({
                name:this.name,
                ingredients:this.ingredients,
                time:this.time,
            })
        }

        publishMenu(){
             let newDiv = document.createElement("div");
             document.body.appendChild(newDiv);
             newDiv.id = "menu";
             newDiv.innerHTML = "MENU";
             newDiv.style.border = "2px solid black";
             let ul = document.createElement("ul");
             newDiv.appendChild(ul);

            function list(element,index,array) {
                if(element){
                    for(let key in element){
                        let item = document.createElement("li");
                        item.innerHTML = key + " = " + element[key];
                        ul.appendChild(item);
                    }
                }
            }
            menu.forEach(list);
        }
    }
    const menu = [];
    const dTime =  10;

    let standardBurger = new Burger("standardBurger");
    standardBurger.addMenu();

    class newBurger extends Burger {
        constructor(name, ingredients, time){
            super(name,ingredients,time);
            }
        addIngredientsBurger(ingr){
            this.ingredients.push(ingr)
        }
    }

    const cheeseBurger = new newBurger("CheeseBurger");
    cheeseBurger.addIngredientsBurger("Сырок");
    cheeseBurger.addMenu();

    const hamburger = new newBurger("Hamburger");
    hamburger.addIngredientsBurger("Помидорка");
    hamburger.addMenu();
    hamburger.publishMenu();

    // console.log(menu);

    class Order {
        constructor(name,condition,value){
            this.name = name;
            this.condition = condition;
            this.value = value;
        }
        search(menu){
            // let {name, condit} =this
            const self = this;
            let x = this.name;
            let y = this.condition;
            let z = this.value;
            menu.map(function (obj) {
                if(obj.name === self.name){
                    console.log(`${obj.name}, будет готов через ${obj.time} минуток`);
                    order.push(obj);
                } else if(y !== "") {
                    obj.ingredients.map(function (ing) {
                        if(ing === y){
                            console.log(`${obj.name} с ${ing} будет готов через ${obj.time} минуток`);
                            order.push(obj);
                        }
                    })
                }
                else if(z === obj.ingredients.map(function (ing) {

                    })
                ){}
            })
        }
    }

    const order =[];
    const orderHamburger = new Order("", "","Помидорка");
    orderHamburger.search(menu);

    const orderCheeseBurger = new Order("CheeseBurger");
    orderCheeseBurger.search(menu);

    const orderChickenBurger = new Order("","Сырок");
    orderChickenBurger.search(menu);
